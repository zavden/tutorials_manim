from manimlib.imports import *

def control_anim_with_alpha(anim,alpha):
    anim.begin()
    anim.interpolate(alpha)

class AlphaExample1(Scene):
    def construct(self):
        circle = Circle(radius=3.5)
        dot = Dot()
        # move the dot to the circle start
        dot.move_to(circle.get_start())
        # dot.move_to(
        #     circle.point_at_angle(45*DEGREES)
        # )
        # always use "alpha" name
        def update_dot(mob,alpha):
            # 0 <= alpha <= 1  ALWAYS
            angle = interpolate(0, 2 * PI, alpha)
            mob.move_to(
                circle.point_at_angle(angle)
            )
            # in this case mob = dot

        self.add(circle,dot)
        self.play(
            UpdateFromAlphaFunc(
                dot, update_dot
            )
        )
        self.wait()
        

class AlphaExample2(Scene):
    def construct(self):
        equation_tex = TexMobject("x^2+y^2=r^2")
        equation_tex.to_corner(UL)
        circle = Circle(radius=3.5)
        dot = Dot()
        dot.move_to(circle.get_start())
        # x value
        x_val = DecimalNumber(0) # this Tex change in the time
        x_val.to_edge(LEFT)
        x_val.add_updater(
            lambda mob: mob.set_value(dot.get_x()/3.5)
        ) #  in this case mob = x_val
        #----------------
        def update_dot(mob,alpha):
            angle = interpolate(0, 2 * PI, alpha)
            mob.move_to(
                circle.point_at_angle(angle)
            )

        self.add(circle,dot,equation_tex,x_val)
        self.play(
            UpdateFromAlphaFunc(
                dot, update_dot,
                rate_func=linear,
                run_time=5
            )
        )
        self.wait()

class ChangeColorBackground(Scene):
    CONFIG = {
        "camera_config": {
            "background_color": RED
        }
    }
    def construct(self):
        self.add(Dot())
